import { APIError } from '../../lib/api_error';
import { controller } from '../../lib/promisify_controller';
import { Item } from '../../models/Item';

/**
 * @api {post} /api/item/ Create new item
 * @apiDescription Creates a new item.
 * @apiName createItem
 * @apiGroup Items
 *
 * @apiParam (Body) {String} name name of the item.
 * @apiParam (Body) {String} price price of the item.
 * @apiParam (Body) {String} [description] description of the item.
 * @apiParam (Body) {String} [category] category of the item.
 *
 * @apiSuccess {string} success returns the ID of the created item
 *
 * @apiError (Error 4xx) 400 Invalid request parameters. Returns the potential reasons.
 * @apiError (Error 4xx) 401 Invalid access token. Not authorized to call the API.
 * @apiError (Error 5xx) 500 Server Error occurred.
 *
 * @apiExample {curl} Example usage:
 * curl -X POST \
 *      -d 'name=Foo&price=10&description=bar' \
 *      -H 'authorization: JWT <token>' \
 *      http://localhost:3000/api/item/
 */
export const createItem = controller(async (req) => {
  req.sanitizeBody('name').trim(' ');
  req.sanitizeBody('category').trim(' ');
  req.sanitizeBody('description').trim(' ');
  const item = {
    author: req.user,
    category: req.body.category,
    description: req.body.description,
    name: req.body.name,
    price: req.body.price,
  };
  try {
    const result = await Item.create(item);
    return result._id;
  } catch (err) {
    if (err.name === 'ValidationError') {
      const message = Object.keys(err.errors).map((error) => err.errors[error].message).join('. ');
      throw new APIError(message);
    }
    throw err;
  }
});
