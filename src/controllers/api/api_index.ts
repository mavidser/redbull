import { controller } from '../../lib/promisify_controller';

export const apiIndex = controller(async (req) => {
  return 'Product API';
});
