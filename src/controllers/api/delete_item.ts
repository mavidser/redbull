import { APIError } from '../../lib/api_error';
import { controller } from '../../lib/promisify_controller';
import { Item } from '../../models/Item';

/**
 * @api {delete} /api/item/:id Delete item
 * @apiDescription Deletes an existing item.
 * @apiName deleteItem
 * @apiGroup Items
 *
 * @apiParam (Param) {String} id ID of the item.
 *
 * @apiSuccess {string} success true if successfully deleted
 *
 * @apiError (Error 4xx) 400 Item not found
 * @apiError (Error 4xx) 403 Not authorized to delete this item
 * @apiError (Error 4xx) 401 Invalid access token. Not authorized to call the API.
 * @apiError (Error 5xx) 500 Server Error occurred.
 *
 * @apiExample {curl} Example usage:
 * curl -X DELETE \
 *      -H 'authorization: JWT <token>' \
 *      http://localhost:3000/api/item/<item_id>
 */
export const deleteItem = controller(async (req) => {
  let item;
  try {
    item = await Item.findById(req.params.id);
    if (!item) {
      throw new APIError('Item not found');
    }
    if (item.author !== req.user) {
      throw new APIError('Not authorized to delete this item', 403);
    }
  } catch (err) {
    if (err instanceof APIError) {
      throw err;
    }
    throw new APIError('Item not found');
  }
  await Item.remove({ _id: req.params.id });
  return true;
});
