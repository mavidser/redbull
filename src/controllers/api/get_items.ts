import { controller } from '../../lib/promisify_controller';
import { validate } from '../../lib/validate_request';
import { Item, ItemModel } from '../../models/Item';

/**
 * @api {get} /api/item Get Items
 * @apiDescription Lists items in the system, based on the specified query.
 * @apiName getItems
 * @apiGroup Items
 *
 * @apiParam (Query) {String} [name] Username of the user.
 * @apiParam (Query) {String} [category] Username of the user.
 * @apiParam (Query) {Boolean} [amAuthor] Only show items created by the current user.
 * @apiParam (Query) {String=name,price} [sortBy=name] Username of the user.
 * @apiParam (Query) {String=asc,desc} [sortOrder=asc] Username of the user.
 *
 * @apiSuccess {Object[]} success List of items
 * @apiSuccess {boolean} [success.amAuthor] True if current user is the author of the item.
 * @apiSuccess {string} success.category Item's category
 * @apiSuccess {string} success.id Item's ID
 * @apiSuccess {string} success.name Item's name
 * @apiSuccess {number} success.price Item's price
 *
 * @apiError (Error 4xx) 400 Invalid request parameters. Returns the potential reasons.
 * @apiError (Error 4xx) 401 Invalid access token. Not authorized to call the API.
 * @apiError (Error 5xx) 500 Server Error occurred.
 *
 * @apiExample {curl} Example usage:
 * curl -X GET \
 *      -H 'authorization: JWT <token>' \
 *      http://localhost:3000/api/item/
 */
export const getItems = controller(async (req) => {
  req.checkQuery('sortOrder', 'invalid sortOrder. Can only be \'asc\' or \'desc\'')
    .optional()
    .matches(/(desc)|(asc)/);

  req.checkQuery('sortBy', 'invalid sortBy. Can only be \'name\' or \'price\'')
    .optional()
    .matches(/name|price/);

  req.checkQuery('amAuthor', 'invalid amAuthor. Can only be boolean')
    .optional()
    .isBoolean();

  await validate(req);
  req.sanitizeQuery('amAuthor').toBoolean();

  if (req.query.sortBy === undefined) {
    req.query.sortBy = 'name';
  }
  if (req.query.sortOrder === undefined) {
    req.query.sortOrder = 'asc';
  }
  const conditions = {} as ItemModel;
  if (req.query.name) {
    (conditions as any).name = new RegExp(`(^${req.query.name})|( ${req.query.name})`, 'i');
  }
  if (req.query.category) {
    (conditions as any).category = new RegExp(`^${req.query.category}$`, 'i');
  }
  if (req.query.amAuthor) {
    (conditions as any).author = req.user;
  }
  const sort = {} as any;
  sort[req.query.sortBy] = req.query.sortOrder === 'asc' ? 1 : -1;
  const res = await Item.find(conditions).sort(sort);
  const responose = res.map((item: any) => ({
    amAuthor: item.author === req.user ? true : undefined,
    category: item.category,
    id: item._id,
    name: item.name,
    price: item.price,
  }));

  return responose;
});
