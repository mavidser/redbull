import { Router } from 'express';
import * as apiController from '../controllers/api/index';

const router = Router();

router.get('/', apiController.apiIndex);
router.get('/item/', apiController.getItems);
router.get('/item/:id', apiController.getItemById);
router.post('/item/', apiController.createItem);
router.put('/item/:id', apiController.updateItem);
router.delete('/item/:id', apiController.deleteItem);

export default router;
