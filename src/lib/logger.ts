import * as winston from 'winston';

const logger = new winston.Logger({
  transports: [
    new winston.transports.Console({
      colorize: true,
      handleExceptions: true,
      humanReadableUnhandledException: true,
      json: false,
      level: 'debug',
    }),
  ],
});

logger.on('error', (err: Error) => {
  console.error(err);
});

export default logger;
