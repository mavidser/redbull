import * as _ from 'lodash';
import * as passport from 'passport';
import { ExtractJwt, Strategy as JwtStrategy } from 'passport-jwt';
import { Strategy as LocalStrategy } from 'passport-local';
import { JWT_SECRET, users } from './mockdata';

// Local strategy for authentication, matching the mock users from the mockdata file
passport.use(new LocalStrategy({ usernameField: 'username', passwordField: 'password' }, (username, password, done) => {
  const user = _.find(users, { username });
  if (user) {
    if (user.password === password) {
      return done(undefined, username);
    } else {
      return done(undefined, false, { message: 'Invalid password.' });
    }
  } else {
    return done(undefined, false, { message: `Username ${username} not found.` });
  }
}));

const jwtOptions = {
  jwtFromRequest: ExtractJwt.fromAuthHeader(),
  secretOrKey: JWT_SECRET,
};

// JWT strategy allows users if the token is valid and unexpired.
passport.use(new JwtStrategy(jwtOptions, (payload, done) => {
  done(undefined, payload.username);
}));

export default passport;
