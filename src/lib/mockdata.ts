// Mock data for demo purposes. Actual storage should be somewhere else.

export const users = [
  {
    password: 'admin',
    username: 'admin',
  },
  {
    password: 'root',
    username: 'root',
  },
];

export const JWT_SECRET = 'correcthorsebatterystaple';
