import { NextFunction, Request, Response } from 'express';

type RouteCallback = (req: Request) => Promise<{}>;

export const controller = (callback: RouteCallback) => {
  return async (req: Request, res: Response, next: NextFunction) => {
    try {
      const out = await callback(req);
      res.json({
        success: out,
      });
    } catch (err) {
      next(err);
    }
  };
};
