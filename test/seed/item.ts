export default [
  {
    author: 'admin',
    category: 'none',
    description: 'Lorem ipsum dolor sit amet',
    name: 'Foo Bar',
    price: 99.99,
  },
  {
    author: 'admin',
    category: 'none',
    description: 'Consectetur adipiscing elit',
    name: 'Foo Bars',
    price: 89.99,
  },
  {
    author: 'admin',
    category: 'none',
    description: 'Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua',
    name: 'Foos Bar',
    price: 79.99,
  },
  {
    author: 'admin',
    category: 'ball',
    description: 'Ut enim ad minim veniam',
    name: 'Foos Ball',
    price: 69.99,
  },
  {
    author: 'admin',
    category: 'ball',
    description: 'Quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat',
    name: 'Foot Ball',
    price: 59.99,
  },
  {
    author: 'root',
    category: 'ball',
    description: 'Duis aute irure dolor in reprehenderit in voluptate velit esse dolore eu fugiat nulla pariatur',
    name: 'Basket Ball',
    price: 49.99,
  },
  {
    author: 'root',
    category: 'ball',
    description: 'Excepteur sint occaecat cupidatat non proident',
    name: 'Volley Ball',
    price: 39.99,
  },
  {
    author: 'root',
    category: 'ball',
    description: 'Sunt in culpa qui officia deserunt mollit anim id est laborum.',
    name: 'Tennis Ball',
    price: 29.99,
  },
  {
    author: 'root',
    category: 'racquet',
    description: 'Very good racquet',
    name: 'Tennis Racquet',
    price: 19.99,
  },
  {
    author: 'root',
    category: 'racquet',
    description: 'Dingoes ate my baby',
    name: 'Squash Racquet',
    price: 9.99,
  }
]
