import * as sinon from 'sinon';
import * as supertest from 'supertest';
import { default as app } from '../src/app';
import { Item } from '../src/models/Item';
import './matchers';
import itemSeed from './seed/item';

const request = supertest(app);

const BAD_REQUEST = 400;
const NOT_AUTHORIZED = 401;
const RESTRICTED = 403;
const SUCCESS = 200;

function sleep(ms) {
  return new Promise((resolve) => setTimeout(resolve, ms));
}

async function getAccessToken(username, password) {
  const res = await request
    .post('/authenticate')
    .send({
      password,
      username,
    });
  expect(res.body.success).toBeInstanceOf(Object);
  expect(typeof res.body.success.access_token).toBe('string');
  return res.body.success.access_token;
}

async function callAPI(statusExpected: number, supertestFunction: any) {
  const accessToken = await getAccessToken('admin', 'admin');
  const res = await supertestFunction
    .set('Authorization', `JWT ${accessToken}`);
  if (statusExpected === SUCCESS) {
    expect(res.statusCode).toBe(SUCCESS);
    return res.body.success;
  } else {
    expect(res.statusCode).toBe(statusExpected);
    return res.body.error;
  }
}

beforeEach(async () => {
  await Item.remove({});
  await Item.create(itemSeed);
});

describe('Basic checks', () => {
  it('GET /', async () => {
    const res = await request
      .get('/');
    expect(res.statusCode).toBe(SUCCESS);
  });
  it('GET /api', async () => {
    const res = await request
      .get('/api');
    expect(res.statusCode).toBe(NOT_AUTHORIZED);
  });
});

describe('Authentication', () => {
  it('No username/password', async () => {
    const res = await request
      .post('/authenticate');
    expect(res.statusCode).toBe(BAD_REQUEST);
  });
  it('Invalid username', async () => {
    const res = await request
      .post('/authenticate')
      .send({
        password: 'admin_wrong',
        username: 'admin_wrong',
      });
    expect(res.statusCode).toBe(NOT_AUTHORIZED);
  });
  it('Wrong password', async () => {
    const res = await request
      .post('/authenticate')
      .send({
        password: 'admin_wrong',
        username: 'admin',
      });
    expect(res.statusCode).toBe(NOT_AUTHORIZED);
  });
  it('Correct username and password', async () => {
    const res = await request
      .post('/authenticate')
      .send({
        password: 'admin',
        username: 'admin',
      });
    expect(res.statusCode).toBe(SUCCESS);
    expect(res.header['content-type']).toMatch(/json/);
    expect(res.body.success).toBeInstanceOf(Object);
    expect(res.body.success.expires_in).toBe(3600);
    expect(res.body.success.token_type).toBe('JWT');
    expect(typeof res.body.success.access_token).toBe('string');
  });
});

describe('Authorization', () => {
  it('Invalid access token', async () => {
    const res = await request
      .get('/api')
      .set('Authorization', 'JWT fake_token');
    expect(res.statusCode).toBe(NOT_AUTHORIZED);
  });
  it('Valid access token', async () => {
    const accessToken = await getAccessToken('admin', 'admin');
    const res = await request
      .get('/api')
      .set('Authorization', `JWT ${accessToken}`);
    expect(res.statusCode).toBe(SUCCESS);
  });
  it('Expired access token', async () => {
    const clock = sinon.useFakeTimers();
    const accessToken = await getAccessToken('admin', 'admin');
    clock.tick(3600100);
    const res = await request
      .get('/api')
      .set('Authorization', `JWT ${accessToken}`);
    expect(res.statusCode).toBe(NOT_AUTHORIZED);
    clock.restore();
  });
});

describe('API | GET /item/', () => {
  it('List all items | Invalid params', async () => {
    await callAPI(BAD_REQUEST, request.get('/api/item?sortOrder=dummy'));
    await callAPI(BAD_REQUEST, request.get('/api/item?sortBy=dummy'));
    await callAPI(BAD_REQUEST, request.get('/api/item?amAuthor=dummy'));
  });
  it('List all items', async () => {
    const response = await callAPI(SUCCESS, request.get('/api/item'));
    expect(response).toBeInstanceOf(Array);
    expect(response).toBeSortedBy('name');
    response.map((item) => {
      expect(item).toBeInstanceOf(Object);
      expect(Object.keys(item).length).toBeGreaterThanOrEqual(4);
      expect(Object.keys(item).length).toBeLessThanOrEqual(5);
      expect(item).toHaveProperty('name');
      expect(item).toHaveProperty('id');
      expect(item).toHaveProperty('category');
      expect(item).toHaveProperty('price');
    });
  });
  it('List all items sorted by name in descending alphabetical order', async () => {
    const response = await callAPI(SUCCESS, request.get('/api/item?sortOrder=desc'));
    expect(response).toBeInstanceOf(Array);
    expect(response).toBeSortedBy('name', false);
  });
  it('List all items sorted by price in ascending order', async () => {
    const response = await callAPI(SUCCESS, request.get('/api/item?sortBy=price&sortOrder=asc'));
    expect(response).toBeInstanceOf(Array);
    expect(response).toBeSortedBy('price');
  });
  it('List all items sorted by price in descending order', async () => {
    const response = await callAPI(SUCCESS, request.get('/api/item?sortBy=price&sortOrder=desc'));
    expect(response).toBeInstanceOf(Array);
    expect(response).toBeSortedBy('price', false);
  });
  it('List all items filtered by name', async () => {
    const response = await callAPI(SUCCESS, request.get('/api/item?name=foo'));
    expect(response).toBeInstanceOf(Array);
    expect(response.length).toBeGreaterThan(1);
    response.map((item) => {
      expect(item.name).toMatch(new RegExp(`(^${item.name})|( ${item.name})`, 'i'));
    });
  });
  it('List all items filtered by category', async () => {
    const response = await callAPI(SUCCESS, request.get('/api/item?category=ball'));
    expect(response).toBeInstanceOf(Array);
    expect(response.length).toBeGreaterThan(1);
    response.map((item) => {
      expect(item.category.toLowerCase()).toBe(item.category.toLowerCase());
    });
  });
  it('List all items created by me', async () => {
    const response = await callAPI(SUCCESS, request.get('/api/item?amAuthor=true'));
    expect(response).toBeInstanceOf(Array);
    expect(response.length).toBeGreaterThan(1);
    response.map((item) => {
      expect(item.amAuthor).toBe(true);
    });
  });
});

describe('API | GET /item/:id', () => {
  it('Get valid item', async () => {
    const responseList = await callAPI(SUCCESS, request.get('/api/item'));
    expect(responseList).toBeInstanceOf(Array);
    const firstId = responseList[0].id;
    const response = await callAPI(SUCCESS, request.get(`/api/item/${firstId}`));
    expect(response).toBeInstanceOf(Object);
    expect(Object.keys(response).length).toBeGreaterThanOrEqual(5);
    expect(Object.keys(response).length).toBeLessThanOrEqual(6);
    expect(response).toHaveProperty('name');
    expect(response).toHaveProperty('id');
    expect(response).toHaveProperty('description');
    expect(response).toHaveProperty('category');
    expect(response).toHaveProperty('price');
    expect(response.id).toBe(firstId);
  });
  it('Get invalid item', async () => {
    await callAPI(BAD_REQUEST, request.get('/api/item/dummyId'));
  });
});

describe('API | POST /item/', () => {
  it('Create new valid item', async () => {
    const response = await callAPI(SUCCESS, request.post('/api/item')
                                                .send({
                                                  category: 'racquetball',
                                                  description: 'Racquet hits ball',
                                                  name: 'Racquetball',
                                                  price: 0.99,
                                                }));
    expect(typeof response).toBe('string');
    const verifyResponse = await callAPI(SUCCESS, request.get(`/api/item/${response}`));
    expect(verifyResponse).toBeInstanceOf(Object);
    expect(Object.keys(verifyResponse)).toHaveLength(6);
    expect(verifyResponse.name).toBe('Racquetball');
    expect(verifyResponse.id).toBe(response);
    expect(verifyResponse.description).toBe('Racquet hits ball');
    expect(verifyResponse.category).toBe('racquetball');
    expect(verifyResponse.price).toBe(0.99);
    expect(verifyResponse.amAuthor).toBe(true);

  });
  it('Create new invalid item | Negative price', async () => {
    const response = await callAPI(BAD_REQUEST, request.post('/api/item')
                                                .send({
                                                  category: 'racquetball',
                                                  description: 'Racquet hits ball',
                                                  name: 'Racquetball',
                                                  price: -0.99,
                                                }));
    expect(response).toBe('Price cannot be less than 0.01');
    const verifyResponse = await callAPI(SUCCESS, request.get(`/api/item?name=Racquetball`));
    expect(verifyResponse).toBeInstanceOf(Array);
    expect(verifyResponse.length).toBe(0);
  });
  it('Create new invalid item | Long name', async () => {
    const response = await callAPI(BAD_REQUEST, request.post('/api/item')
                                                .send({
                                                  category: 'racquetball',
                                                  description: 'Racquet hits ball',
                                                  name: 'QWERTYUIOPASDFGHJKLZXCVBNMNBVCXZLKJHGFDSAPOIUYTREWQ',
                                                  price: 0.99,
                                                }));
    expect(response).toBe('Name cannot be more than 50 characters');
    const verifyResponse = await callAPI(SUCCESS, request.get(`/api/item?name=QWERTY`));
    expect(verifyResponse).toBeInstanceOf(Array);
    expect(verifyResponse.length).toBe(0);
  });
  it('Create new invalid item | Long category', async () => {
    const response = await callAPI(BAD_REQUEST, request.post('/api/item')
                                                .send({
                                                  category: 'QWERTYUIOPASDFGHJKLZXCVBNMNBVCXZLKJHGFDSAPOIUYTREWQ',
                                                  description: 'Racquet hits ball',
                                                  name: 'Racquetball',
                                                  price: 0.99,
                                                }));
    expect(response).toBe('Category cannot be more than 50 characters');
    const verifyResponse = await callAPI(SUCCESS, request.get(`/api/item?name=Racquetball`));
    expect(verifyResponse).toBeInstanceOf(Array);
    expect(verifyResponse.length).toBe(0);
  });
  it('Create new invalid item | No name', async () => {
    const response = await callAPI(BAD_REQUEST, request.post('/api/item')
                                                .send({
                                                  category: 'racquetball',
                                                  description: 'Racquet hits ball',
                                                  price: 0.99,
                                                }));
    expect(response).toBe('Name is required for the item');
    const verifyResponse = await callAPI(SUCCESS, request.get(`/api/item?category=racquetball`));
    expect(verifyResponse).toBeInstanceOf(Array);
    expect(verifyResponse.length).toBe(0);
  });
  it('Create new invalid item | No price', async () => {
    const response = await callAPI(BAD_REQUEST, request.post('/api/item')
                                                .send({
                                                  category: 'racquetball',
                                                  description: 'Racquet hits ball',
                                                  name: 'Racquetball',
                                                }));
    expect(response).toBe('Price is required for the item');
    const verifyResponse = await callAPI(SUCCESS, request.get(`/api/item?name=Racquetball`));
    expect(verifyResponse).toBeInstanceOf(Array);
    expect(verifyResponse.length).toBe(0);
  });
});

describe('API | PUT /item/:id', () => {
  it('Update item authorized for', async () => {
    const responseList = await callAPI(SUCCESS, request.get('/api/item?amAuthor=1'));
    expect(responseList).toBeInstanceOf(Array);
    const firstId = responseList[0].id;
    const response = await callAPI(SUCCESS, request.put(`/api/item/${firstId}`)
                                                .send({
                                                  category: 'racquetball',
                                                  description: 'Racquet hits ball',
                                                  name: 'Racquetball',
                                                  price: 0.99,
                                                }));
    expect(response).toBe(true);
    const verifyResponse = await callAPI(SUCCESS, request.get(`/api/item/${firstId}`));
    expect(verifyResponse).toBeInstanceOf(Object);
    expect(Object.keys(verifyResponse)).toHaveLength(6);
    expect(verifyResponse.name).toBe('Racquetball');
    expect(verifyResponse.id).toBe(firstId);
    expect(verifyResponse.description).toBe('Racquet hits ball');
    expect(verifyResponse.category).toBe('racquetball');
    expect(verifyResponse.price).toBe(0.99);
    expect(verifyResponse.amAuthor).toBe(true);
  });
  it('Update item not authorized for', async () => {
    const responseList = await callAPI(SUCCESS, request.get('/api/item'));
    expect(responseList).toBeInstanceOf(Array);
    const firstId = responseList[0].id;
    const response = await callAPI(RESTRICTED, request.put(`/api/item/${firstId}`)
                                                .send({
                                                  category: 'racquetball',
                                                  description: 'Racquet hits ball',
                                                  name: 'Racquetball',
                                                  price: 0.99,
                                                }));
    expect(response).toBe('Not authorized to modify this item');
    const verifyResponse = await callAPI(SUCCESS, request.get(`/api/item/${firstId}`));
    expect(verifyResponse).toMatchObject(responseList[0]);

  });
  it('Update non-existent item', async () => {
    const response = await callAPI(BAD_REQUEST, request.put('/api/item/dummyId')
                                                .send({
                                                  category: 'racquetball',
                                                  description: 'Racquet hits ball',
                                                  name: 'Racquetball',
                                                  price: 0.99,
                                                }));
    expect(response).toBe('Item not found');
    const verifyResponse = await callAPI(BAD_REQUEST, request.get(`/api/item/dummyId`));
    expect(verifyResponse).toBe('Item not found');
  });
  it('Update item authorized for with invalid data | negative price', async () => {
    const responseList = await callAPI(SUCCESS, request.get('/api/item?amAuthor=1'));
    expect(responseList).toBeInstanceOf(Array);
    const firstId = responseList[0].id;
    const response = await callAPI(BAD_REQUEST, request.put(`/api/item/${firstId}`)
                                                .send({
                                                  category: 'racquetball',
                                                  description: 'Racquet hits ball',
                                                  name: 'Racquetball',
                                                  price: -0.99,
                                                }));
    expect(response).toBe('Price cannot be less than 0.01');
    const verifyResponse = await callAPI(SUCCESS, request.get(`/api/item/${firstId}`));
    expect(verifyResponse).toMatchObject(responseList[0]);
  });
});

describe('API | DELETE /item/:id', () => {
  it('Delete item authorized for', async () => {
    const responseList = await callAPI(SUCCESS, request.get('/api/item?amAuthor=1'));
    expect(responseList).toBeInstanceOf(Array);
    const firstId = responseList[0].id;
    const response = await callAPI(SUCCESS, request.delete(`/api/item/${firstId}`));
    expect(response).toBe(true);
    const verifyResponse = await callAPI(BAD_REQUEST, request.get(`/api/item/${firstId}`));
    expect(verifyResponse).toBe('Item not found');
  });
  it('Delete item not authorized for', async () => {
    const responseList = await callAPI(SUCCESS, request.get('/api/item'));
    expect(responseList).toBeInstanceOf(Array);
    const firstId = responseList[0].id;
    const response = await callAPI(RESTRICTED, request.delete(`/api/item/${firstId}`));
    expect(response).toBe('Not authorized to delete this item');
    const verifyResponse = await callAPI(SUCCESS, request.get(`/api/item/${firstId}`));
    expect(verifyResponse).toMatchObject(responseList[0]);
  });
  it('Delete non-existent item', async () => {
    const response = await callAPI(BAD_REQUEST, request.delete('/api/item/dummyId'));
    expect(response).toBe('Item not found');
    const verifyResponse = await callAPI(BAD_REQUEST, request.get(`/api/item/dummyId`));
    expect(verifyResponse).toBe('Item not found');
  });
});
