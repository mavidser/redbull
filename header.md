This API is built upon [MongoDB](https://docs.mongodb.com/manual/installation/) and [NodeJS 6+](https://docs.mongodb.com/manual/installation/).

It uses a REST pattern for API Usage. Only authenitcated users are allowed to access the API.

The key can be obtained from the `/authenticate` endpoint as specified below. This key must be sent along with every supported API request.

This API is a proof-of-concept for building an API server, and hence has several limitations as specified:

- No users database: Two mock users have been hardcoded in the API. The usernames are `admin` and `root` with the same being the passwords too. Credential storage takes a lot of implementation which seems out of scope for this task. One can start with using the database to store passwords with bcrypt.

- This implementation is only the app server part. No performance enhancements like clustering, compression, caching, etc has been implemented. In ideal cases, the application will be deployed across many cores, behind a nginx reverse proxy to take care of those parts.

- Querying for items can have many filters and constraints, each with their own implementations. This API handles only the most basic of those for demo purposes.

- A proper API should return pageinated response for list of objects. This hasn't been implemented here.

- Some basic errors have been handled, but this API might need more error messages for a varierty of cases.
